/*
 * Copyright (C) 2004-2016 L2J Server
 * 
 * This file is part of L2J Server.
 * 
 * L2J Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.perenneCustom;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.database.pool.impl.ConnectionFactory;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;

/**
 * @author Rizaar
 */
public class PerennePlayerStore
{
	private static final String LOAD_CUSTOM_VARIABLE = "SELECT * FROM CUSTOM_VARIABLE WHERE ID = ?";
	private static final String UPDATE_CUSTOM_VARIABLE = "INSERT INTO CUSTOM_VARIABLE (ID,name,value) VALUES(?,?,?) ONE DUPLICATE KEY UPDATE value=?";
	
	private static final Logger LOG = LoggerFactory.getLogger(PerennePlayerStore.class);
	
	public static void loadCustomVariable(L2PcInstance activeChar)
	{
		try (Connection con = ConnectionFactory.getInstance().getConnection();
			PreparedStatement ps = con.prepareStatement(LOAD_CUSTOM_VARIABLE))
		{
			// Retrieve all skills of this L2PcInstance from the database
			ps.setInt(1, activeChar.getObjectId());
			try (ResultSet rs = ps.executeQuery())
			{
				while (rs.next())
				{
					activeChar.getPerennePlayer().addOrUpdateVariableTemporaire(rs.getString("name"), rs.getString("value"));
				}
			}
		}
		catch (Exception e)
		{
			LOG.error("Could not restore custom Variable : {}", e);
		}
	}
	
	public static void saveCustomVariable(L2PcInstance activeChar)
	{
		try (Connection con = ConnectionFactory.getInstance().getConnection())
		{
			
			for (String s : activeChar.getPerennePlayer().getVariableTemporaire().keySet())
			{
				
				try (PreparedStatement ps = con.prepareStatement(UPDATE_CUSTOM_VARIABLE))
				{
					ps.setInt(1, activeChar.getObjectId());
					ps.setString(2, s);
					ps.setString(3, activeChar.getPerennePlayer().getVariableTemporaire(s));
					ps.setString(4, activeChar.getPerennePlayer().getVariableTemporaire(s));
					ps.execute();
				}
				
			}
		}
		catch (Exception e)
		{
			LOG.warn("Error could not store custom Variable: {}", e);
		}
	}
}
